<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class orderController extends Controller
{
    /**
     * Undocumented function
     *
     * @param integer $id
     * @return void
     */
    public function order(int $id)
    {
        $order = Order::with('user', 'products', 'payments')->findOrFail($id);
        return response()->json($order);
    }


    /**
     * Undocumented function
     *
     * @param integer $userid
     * @return void
     */
    public function userOrders(int $userid)
    {
        $user = Auth::user();
        if ($user->id == $userid) {
            $user_orders = Order::with('user', 'products', 'payments')->where('user_id', $userid)->get();
            return response()->json($user_orders);
        } else {
            return response()->json('Unauthorized');
        }
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function createOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userid' => 'required',
            'products' => 'required',
            'payments' => 'required',
            'price' => 'required'
        ]);

        $order = new Order([
            'user_id' => $request->userid,
            'price' => $request->price
        ]);

        $order->save();
        $order->products()->attach($request->products);
        $order->payments()->attach($request->payments);
        return response()->json("Order added successfully");
    }

    public function updateOrder(Request $request, $orderid)
    {
        $order = Order::find($orderid);
        $validator = Validator::make($request->all(), [
            'products' => 'nullable'
        ]);
        $order->products()->sync($request->products);
        return response()->json("Order Updated successfully");
    }
}
