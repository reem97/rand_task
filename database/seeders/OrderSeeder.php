<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Order;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $order = Order::create([
            'user_id' => '1',
            'price' => '70000'
        ]);
        $order->products()->attach([1, 2]);
        $order->payments()->attach([1, 2]);
    }
}
