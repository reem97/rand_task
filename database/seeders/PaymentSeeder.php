<?php

namespace Database\Seeders;

use App\Models\Payment;
use Illuminate\Database\Seeder;

class PaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $payment = Payment::create([
            'paymenttype' => 'creditcard'
        ]);
        $payment1 = Payment::create([
            'paymenttype' => 'wiretransfer'
        ]);
    }
}
