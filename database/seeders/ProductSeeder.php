<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product = Product::create(
            [
                'productname' => 'Skirt'
            ]
        );
        $product1 = Product::create(
            [
                'productname' => 'T-shirt'
            ]
        );
        $product2 = Product::create(
            [
                'productname' => 'Pants'
            ]
        );
        $product3 = Product::create(
            [
                'productname' => 'Dress'
            ]
        );
    }
}
