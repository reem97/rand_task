<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('auth/register', 'AuthController@register');
Route::post('auth/login', 'AuthController@login');


Route::middleware('auth:api')->group(function () {
    Route::get('orders/{id}', 'orderController@order');
    Route::get('orders/userid/{userid}', 'orderController@userOrders');
    Route::post('orders', 'orderController@createOrder');
    Route::put('orders/{orderid}', 'orderController@updateOrder');
});
